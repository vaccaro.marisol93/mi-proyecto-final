class Empresas {
    constructor(nombre, localidad, tipo, puestos) {
        this.nombre = nombre;
        this.localidad = localidad;
        this.tipo = tipo;
        this.puestos = puestos;
    }
}

let empresa1 = new Empresas(
    "Telefonica",
    "CABA",
    "Comunicaciones",
    "Dev JR"
)

export default Empresas