class Empleo {
    constructor(empresa, puesto, localidad, fecha, requisitos) {
        this.empresa = empresa;
        this.puesto = puesto;
        this.localidad = localidad;
        this.fecha = fecha;
        this.requisitos = requisitos;
    }
}

let empleo1 = new Empleo(
    "Globant",
    "Desarrollador",
    "CABA",
    "4 de Julio 2021",
    "Al menos 6 meses de exp",
)